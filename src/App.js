import React from 'react'
import './App.css'
import HeaderLayout from './components/header/Header'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import Home from './components/home page/Home'
import About from './components/about page/About'
import Runs from './components/score page/Runs'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Fotter from './components/Fotter/Fotter'
import Notfound from './components/NotFound/Notfound'

function App () {
  return (
    <div className="App">
      {/* <Header/> */}
      <Router>
        <Routes>
         <Route path='/' element={<Home/>}/>
       <Route path="/about" element={<HeaderLayout/>}>
         <Route index element={<About />} />
         <Route path="runs" element={<Runs />} />
       </Route>
       <Route path="*" element={<Notfound />} />
        </Routes>
      </Router>
      <Fotter/>
    </div>
  )
}

export default App
