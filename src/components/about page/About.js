import React from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import './About.css'

function About () {
  const value = useLocation()

  const navigate = useNavigate()

  const GotoRuns = (e, value) => {
    navigate('/about/runs', { state: { value } })
  }

  return (
    <>
    <div className="card mb-3">
    <img id='about-img' className="card-img-top" src={value.state.value.cAboutimg} alt="Card image cap"/>
    <div className="card-body">
      <div>NO#<span>{value.state.value.id}</span></div>
      <h5 className="card-title">{value.state.value.cName}</h5>
      <p className="card-text">{value.state.value.cDecription}</p>
     <button onClick={(e, _id) => GotoRuns(e, value)}>View Runs</button>
    </div>
  </div>
    </>
  )
}

export default About
