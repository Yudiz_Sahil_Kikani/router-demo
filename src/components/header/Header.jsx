import React from 'react'
import './Header.css'
import { Link, Outlet } from 'react-router-dom'

function HeaderLayout () {
  return (
    <div>

<nav id="navbar" className="navbar navbar-expand-lg navbar-light">

  <div className="container-fluid">

    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <a className="navbar-brand mt-2 mt-lg-0" href="#">
        {/* <img
        //   src="https://th.bing.com/th/id/OIP.mh8gU5BmcNm2hWaTinrj-AHaGe?pid=ImgDet&rs=1"
          height="35"
          alt="MDB Logo"
          loading="lazy"
        /> */}
      </a>
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
       <Link to='/'><a className="nav-link" href="#">Home</a></Link>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">About</a>
        </li>
        <li className="nav-item">
       <a className="nav-link" href="#">Runs</a>
        </li>
      </ul>
    </div>

    <div className="d-flex align-items-center">

      <a className="text-reset me-3" href="#">
        <i className="fas fa-shopping-cart"></i>
      </a>

      <div className="dropdown">
        <a>
          <img
            src="https://mdbcdn.b-cdn.net/img/new/avatars/2.webp"
            className="rounded-circle"
            height="25"
            alt="Black and White Portrait of a Man"
            loading="lazy"
          />
        </a>
       </div>
    </div>

  </div>

</nav>
<Outlet/>
    </div>
  )
}

export default HeaderLayout
