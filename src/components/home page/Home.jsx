import { React, useState, useEffect } from 'react'
import './Home.css'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import HeaderLayout from '../header/Header'

function Home () {
  const Api = axios.create({
    baseURL: 'https://6364b00c8a3337d9a2fc18f2.mockapi.io/crickdata/'
  })
  const navigate = useNavigate()

  const [apiData, setApiData] = useState([])

  const crickdata = () => {
    Api.get('')
      .then((getData) => {
        setApiData(getData.data)
      })
  }

  const handleClick = (e, value) => {
    navigate('/about', { state: { value } })
  }

  useEffect(() => {
    crickdata()
  }, [])

  return (
    <>
    <HeaderLayout/>
    <div className='head'>
        <h1>Most Runs in t20</h1>
    </div>
        <div className="row">
          {/* <div id="live-score" className="col-lg-4">
            <h2>Live Scores</h2>
          </div> */}
          <div id="read-list" className="col-lg-4">
            <h2>Read list</h2>
            <p>
              Video Of Spectators Urvashi Taunt At Rishabh Pant Slammed By
              Twitter
            </p>
            <br />
            <p>
              T20 World Cup, 1st Semi-final Preview: Unpredictable Pakistan To
              Test Consistent New Zealand
            </p>
            <br />
            <p>
              Injury Scare For Rohit Sharma During Practice, Resumes Training
              Ahead Of T20 World Cup Semi-Final: Report
            </p>
            <div className="card">
  <div className="card-body">
    <h5 className="card-title">FEATURED MATCHES</h5>
    <h6 className="card-subtitle matchTitle mb-2 text-muted">IND vs ENG</h6>
    <p className="card-text teamName">Ind  </p>
    <p className="card-text teamName">Eng  </p>
    <h6 className="card-subtitle matchTitle mb-2 text-muted">NZ vs PAK</h6>
    <p className="card-text teamName">Nz - 152/4(20)</p>
    <p className="card-text teamName">Pak - 153/3(19.1)</p>
    <h6 className="card-subtitle matchTitle mb-2 text-muted">IND vs ZIM</h6>
    <p className="card-text teamName">IND - 186/5(20)</p>
    <p className="card-text teamName">ZIM - 115(17.2)</p>
  </div><br/>
  <div>
  <h5>India v England, Semi Final 2: Preview</h5>
  <iframe width="300" height="200" src="https://www.youtube.com/embed/lQRKDgLj2Ek"
   title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
   </div><br/>
   <div>
  <h5>Answered: Was Virat Kohli guilty of fake fielding during Ind-Ban clash?</h5>
  <iframe width="300" height="200" src="https://www.youtube.com/embed/rYrrwvUObP8" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"allowfullscreen></iframe>
   </div><br/>
   <div>
  <h5>Comm Box: T20 WC | New Zealand v Pakistan, 1st SF, 1st innings</h5>
  <iframe width="300" height="200" src="https://www.youtube.com/embed/lIHEUpBesio" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
   </div>
</div>
          </div>

          {apiData.map((value, index) => {
            return (
                <div key={index} className='detail' onClick={(e, _id) => handleClick(e, value)}>
              <div id="player-detail" className="col-lg-4">
                {/* <h1>Top 10 players in T20 wordcup</h1> */}
                <br />
                <div className="card-body">
                  <h5 className="card-title">{value.cName}</h5>
                  <p className="card-text">
                    <small className="text-muted">
                      {value.cTeam}
                    </small>
                  </p>
                </div>
                <img
                   id="Ppic"
                  className="card-img-bottom"
                  src= {value.cProfileimg}
                  alt="Card image cap"
                />
              </div>
              </div>
            )
          })}

         </div>
             </>
  )
}

export default Home
