import React from 'react'
import { useLocation } from 'react-router-dom'

function Runs () {
  const Data = useLocation()
  console.log(Data)

  return (
    <table className="table">
      <thead><h2>{Data.state.value.state.value.cName} Runs</h2></thead><br/>
    <tbody>
    <tr>
      <th scope="row">Runs</th>
      <td>{Data.state.value.state.value.cRuns}</td>
    </tr>
    <tr>
      <th scope="row">Innings</th>
      <td>{Data.state.value.state.value.cIngs}</td>
    </tr>
    <tr>
      <th scope="row">Avrage</th>
      <td>{Data.state.value.state.value.cAvg}</td>
    </tr>
    <tr>
      <th scope="row">Strikerate</th>
      <td>{Data.state.value.state.value.cStrike}</td>
    </tr>
    <tr>
      <th scope="row">HigheScore</th>
      <td>{Data.state.value.state.value.cHighscore}</td>
    </tr>
    <tr>
      <th scope="row">50+</th>
      <td>{Data.state.value.state.value.cFifty}</td>
    </tr>
  </tbody>
</table>
  )
}

export default Runs
